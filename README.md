# 小练习：用CSS画一个彩虹
- 主要练习用overflow:hidden;解决父子margin外边距合并

# 笔记
## 1、margin合并
- 父子margin存在合并情况
- 兄弟margin也存在合并情况

## 2、如何阻止合并
- 父子用padding/border/父子用overflow: hidden;挡住
- 兄弟合并一般符合预期，要消除可以使用inline-block
